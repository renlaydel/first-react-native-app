import React, { useState } from 'react';
import { Button } from 'react-native';
import Container from './src/components/common/Container';
import GoalList from './src/components/GoalList';
import GoalInput from './src/components/GoalInput';

export default function App() {
  const [currentGoals, setCurrentGoals] = useState([]);
  const [isModalOpen, setModalOpen] = useState(false);

  const addGoalHandler = (inputGoal) => {
    if (inputGoal) {
      setCurrentGoals(currentGoals => [
        ...currentGoals,
        inputGoal,
      ]);
      modalHandler();
    }
  }

  const removeGoalHandler = (goal) => {
    setCurrentGoals(currentGoals => {
      currentGoals.splice(goal.index, 1);
      return [...currentGoals];
    });
  };

  const modalHandler = () => {
    setModalOpen(isModalOpen => !isModalOpen);
  };

  return (
    <Container>
      <GoalInput
        visible={isModalOpen}
        onAdd={addGoalHandler}
        onCancel={modalHandler}
      />
      <Button
        title="Add New Goal"
        onPress={modalHandler}
      />
      <GoalList
        data={currentGoals}
        onRemoveGoal={removeGoalHandler}
      />
    </Container>
  );
}
