import React from 'react';
import { StyleSheet, View, Text, TouchableHighlight } from 'react-native';

export default function GoalListItem(props) {
  const { data } = props;
  return (
    <TouchableHighlight
      activeOpacity={1}
      style={styles.listItem}
      underlayColor="#e3e3e3"
      onPress={() => props.onRemove(data)}
    >
      <Text>
        {data.item}
      </Text>
    </TouchableHighlight>
  )
}

const styles = StyleSheet.create({
  listItem: {
    padding: 10,
    margin: 5,
    borderColor: 'black',
    borderWidth: 0.5,
    borderRadius: 5,
  },
});
