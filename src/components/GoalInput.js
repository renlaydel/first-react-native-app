import React, { useState } from 'react';
import { StyleSheet, View, Button, Modal } from 'react-native';
import FieldInput from './common/FieldInput';

export default function GoalInput(props) {
  const [inputGoal, setGoal] = useState('');

  const goalInputHandler = (goal) => {
    setGoal(goal);
  };

  const addGoalHandler = () => {
    props.onAdd(inputGoal);
    setGoal('');
  };

  const cancelGoalHandler = () => {
    props.onCancel();
    setGoal('');
  };

  return (
    <Modal
      visible={props.visible}
      animationType="slide"
    >
      <View style={styles.view}>
        <FieldInput
          autoFocus
          clearButtonMode="while-editing"
          placeholder="Course Goal"
          style={styles.input}
          value={inputGoal}
          onChangeText={goalInputHandler}
        />
        <View style={styles.buttonContainer}>
          <Button
            title="CANCEL"
            color="red"
            onPress={cancelGoalHandler}
          />
          <Button
            title="ADD"
            onPress={addGoalHandler}
          />
        </View>
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  view: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  input: {
    width: '80%'
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
});
