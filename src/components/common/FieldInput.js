import React from 'react';
import { StyleSheet, TextInput } from 'react-native';

class FieldInput extends TextInput {
  render() {
    return (
      <TextInput
        {...this.props}
        style={{
          ...styles.input,
          ...this.props.style,
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  input: {
    borderColor: 'black',
    borderWidth: 0.5,
    borderRadius: 5,
    padding: 12,
  },
});

export default FieldInput;