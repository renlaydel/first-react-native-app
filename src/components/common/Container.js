import React from 'react';
import { StyleSheet, View } from 'react-native';

export default function Container(props) {
  const { style, children } = props;
  return (
    <View style={{
      ...style,
      ...styles.container
    }}>
      {children}
    </View>
  );

}

const styles = StyleSheet.create({
  container: {
    padding: 50,
  },
});
