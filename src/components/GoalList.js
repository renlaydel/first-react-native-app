import React from 'react';
import { FlatList } from 'react-native';
import GoalListItem from './GoalListItem';

export default function GoalList(props) {
  return (
    <FlatList
      data={props.data}
      keyExtractor={(item, index) => index + ''}
      renderItem={(itemData) =>
        <GoalListItem
          data={itemData}
          onRemove={props.onRemoveGoal}/>
      }
    />
  );

}